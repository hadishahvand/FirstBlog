from django.shortcuts import render, get_object_or_404, redirect
from .models import Post,Category,Tag
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.utils import timezone 
from django.views.generic import (
    ListView,
    DetailView, 
    CreateView,
    UpdateView,
    DeleteView,
    TemplateView,
)
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import login_required
from users.models import Profile
from .forms import PostForm
from django.contrib import messages
import json



def like_post(request):
    post = get_object_or_404(Post, id=request.POST.get('post_id'))
    redirect_url = request.POST.get('redirect_url', '/')
    is_liked = False
    if post.likes.filter(id=request.user.id).exists():
        post.likes.remove(request.user)
        is_liked = False
    else:
        post.likes.add(request.user)
        is_liked = True
    return HttpResponseRedirect(redirect_url)


class PostListView(ListView):
    model = Post
    template_name = 'blog/home.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']
    queryset = Post.objects.filter(public=True)
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tmp = []
        for post in context['posts']:
            tmp.append({
                'obj': post,
                'is_liked': post.is_liked_by_user(self.request.user)
            })
        context['posts'] = tmp
        return context

    
class MyPostListView(ListView):
    model = Post
    template_name = 'blog/my_posts.html'
    context_object_name = 'posts'
    paginate_by = 5


    def get_queryset(self): 
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        if self.request.user == user:
            return Post.objects.filter(author=user).order_by('-date_posted')
        else:
            return Post.objects.filter(author=user).order_by('-date_posted')


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tmp = []
        for post in context['posts']:
            tmp.append({
                'obj': post,
                'is_liked': post.is_liked_by_user(self.request.user)
            })
        context['posts'] = tmp
        return context


class UserPostListView(ListView):
    model = Post
    template_name = 'blog/user_posts.html'
    context_object_name = 'posts'
    paginate_by = 5



    def get_queryset(self): 
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        if self.request.user == user:
            return Post.objects.filter(author=user).order_by('-date_posted')
        else:
            return Post.objects.filter(author=user, public=True).order_by('-date_posted')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tmp = []
        for post in context['posts']:
            tmp.append({
                'obj': post,
                'is_liked': post.is_liked_by_user(self.request.user)
            })
        context['posts'] = tmp
        return context


class TagPostListView(ListView):
    model = Post
    template_name = 'blog/tag_posts.html'
    context_object_name = 'posts'
    paginate_by = 5


    def get_queryset(self): 
        tag = get_object_or_404(Tag, name=self.kwargs.get('username'))
        
        return tag.tag.all().order_by('-date_posted')


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tmp = []
        for post in context['posts']:
            tmp.append({
                'obj': post,
                'is_liked': post.is_liked_by_user(self.request.user)
            })
        context['posts'] = tmp
        return context




class CategoryPostListView(ListView):
    model = Post
    template_name = 'blog/category_posts.html'
    context_object_name = 'posts'
    paginate_by = 5


    def get_queryset(self): 
        category = get_object_or_404(Category, name=self.kwargs.get('username'))
        
        return category.posts.all().order_by('-date_posted')


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tmp = []
        for post in context['posts']:
            tmp.append({
                'obj': post,
                'is_liked': post.is_liked_by_user(self.request.user)
            })
        context['posts'] = tmp
        return context


class PostDetailView(DetailView):
    model = Post
    

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        post = self.get_object()
        if post.likes.filter(id=self.request.user.id).exists():
            context['is_liked'] = True
            
        else:
            context['is_liked'] = False
        context['total_likes'] = post.total_likes
        return context


class NewPostView(TemplateView):
    template_name = 'blog/post_form.html'

    def get(self, request):
        form = PostForm(request.POST, request.FILES)
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            user_tags = form.cleaned_data.get('tags', [])
            for tag in user_tags:
                tag, _ = Tag.objects.get_or_create(name=tag)
                post.tag.add(tag)
            post.save()
            messages.success(request, 'Your Post Created')
            
        return render(request, self.template_name, {'form':form})


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post 
    fields = ['title','category','tag', 'content','image', 'public']
    template_name_suffix = '_update_form'
    

    def form_valid(self, form):
        form.instance.author = self.request.user     
        return super().form_valid(form)


    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False
    

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url= '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False
    

def about(request):
    return render(request, 'blog/about.html', {'title': 'about'})


def welcome(request):
    return render(request, 'blog/welcome.html', {'title': 'welcome'})


def UserProfile (request, username):
    user = User.objects.get(username=username)
    if request.user.username == username:
        return redirect ('my-profile')
    return render(request,'users/profile-user.html', {"user":user})

def tags_typeahead(request):
    query = request.GET.get('q', None)
    tags = Tag.objects.filter(name__icontains=query).values_list('name', flat=True)
    tags_list = [t for t in tags]
    return JsonResponse(tags_list, safe=False)