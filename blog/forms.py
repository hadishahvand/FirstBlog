from django import forms
from django.forms import ModelForm
from blog.models import Post
from django.contrib.postgres.forms import SimpleArrayField


class PostForm(forms.ModelForm):
    tags = SimpleArrayField(forms.CharField(widget=forms.TextInput(
        attrs={
            'size':'50'
        }
    )))

    class Meta:
        model = Post
        fields = ('title','category','tag','content','image','public')

