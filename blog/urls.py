from django.urls import path
from django.conf.urls import url
from .views import (
    PostListView, 
    PostDetailView,
    PostUpdateView,
    PostDeleteView,
    UserPostListView,
    CategoryPostListView,
    MyPostListView,
    TagPostListView,
)
from . import views 
from django.contrib.auth.decorators import login_required
from blog import views
from django.conf import settings
from django.conf.urls.static import static


 
urlpatterns = [
    path('', login_required(PostListView.as_view()), name='blog-home'),
    path('user/<str:username>/posts/', UserPostListView.as_view(), name='user-posts'),
    path('category/<str:username>/', CategoryPostListView.as_view(), name='category-posts'),
    path('tag/<str:username>/', TagPostListView.as_view(), name='tag-posts'),
    path('post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
    path('post/new/', views.NewPostView.as_view(), name='post-create'),
    path('user/<str:username>/posts/', MyPostListView.as_view(), name='my-post'),
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),
    path('user/<str:username>/profile/', views.UserProfile, name='user-profile'),
    path('about/', views.about, name='blog-about'),
    path('welcome/', views.welcome, name='blog-welcome'),
    path('tags-typeahead/', views.tags_typeahead, name='tags-typeahead'),
] 


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

 