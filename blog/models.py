from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse





class Category(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Post(models.Model):
    title = models.CharField(max_length=100)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='posts')
    content = models.TextField()
    image = models.ImageField(upload_to='post_images', blank=True)
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    public = models.BooleanField(default=True)
    likes = models.ManyToManyField(User, related_name='likes', blank=True)
    tag  = models.ManyToManyField(Tag, related_name='tag', blank=True)

    def __str__(self):
        return self.title
        

    def get_absolute_url(self):
        return reverse("post-detail", kwargs={"pk": self.pk})
    
    def is_liked_by_user(self, user):
        return user in self.likes.all()

    @property
    def total_likes(self):
        return self.likes.count()
    

    




